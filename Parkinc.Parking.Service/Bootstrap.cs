﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Autofac;
using EasyNetQ;
using EasyNetQ.Serilog;
using Flurl.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Parkinc.Parking.Core.Helpers;
using Parkinc.Parking.Core.Helpers.Interface;
using Parkinc.Parking.Core.Integrations;
using Parkinc.Parking.Core.Integrations.Interfaces;
using Serilog;
using StackExchange.Redis;
using IContainer = Autofac.IContainer;

namespace Parkinc.Parking.Service
{
    public static class Bootstrap
    {
        public static IContainer InitializeContainer()
        {
            var builder = new ContainerBuilder();

            builder.Register(ctx => GetConfiguration())
                .As<IConfiguration>()
                .SingleInstance();
            builder.Register(ctx => LogInitializer.InitializeLogger(ctx.Resolve<IConfiguration>()["Logging:LogzToken"],
                                                                    ctx.Resolve<IConfiguration>()["Logging:AwsElasticsearchUrl"]))
                .As<ILogger>()
                .SingleInstance();

            builder.Register(ctx => new FlurlClient(ctx.Resolve<IConfiguration>()["UcnParkingDataProvider:Hostname"]))
                .As<IFlurlClient>()
                .SingleInstance();

            builder.Register(ctx => new RedisCache(ConnectionMultiplexer.Connect(ctx.Resolve<IConfiguration>()["Redis:Host"])))
                .As<IRedisCache>();

            builder.RegisterType<UcnParkingDataProvider>()
                .As<IParkingDataProvider>()
                .SingleInstance();

            builder.Register(ctx => RabbitHutch.CreateBus(
                ctx.Resolve<IConfiguration>()["Messaging:ConnectionString"],
                register => register.Register<IEasyNetQLogger>(sp => new SerilogLogger(ctx.Resolve<ILogger>()))
                ))
                .As<IBus>()
                .SingleInstance();
            builder.RegisterType<ParkingServiceManager>()
                .As<IServiceManager>()
                .SingleInstance();

            return builder.Build();
        }

        private static IConfiguration GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            return builder.Build();
        }
    }
}
