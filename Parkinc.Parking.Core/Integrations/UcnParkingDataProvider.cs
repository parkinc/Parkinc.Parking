﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;
using Flurl.Http;
using Microsoft.Extensions.Caching.Memory;
using Parkinc.Parking.Core.Constants;
using Parkinc.Parking.Core.Helpers.Interface;
using Parkinc.Parking.Core.Integrations.DTOs;
using Parkinc.Parking.Core.Integrations.Interfaces;
using Parkinc.Parking.Core.Mappings;
using Parkinc.Parking.Core.Models;
using Polly;
using Polly.Caching;
using Serilog;

namespace Parkinc.Parking.Core.Integrations
{
    public class UcnParkingDataProvider : IParkingDataProvider
    {
        private readonly IFlurlClient _httpClient;
        private readonly ILogger _logger;
        private readonly IRedisCache _cache;
        private readonly Random _random = new Random();
        private readonly object _lock = new object();


        public UcnParkingDataProvider(IFlurlClient httpClient, ILogger logger, IRedisCache cache)
        {
            _httpClient = httpClient;
            _logger = logger;
            _cache = cache;
        }

        public async Task<IEnumerable<ParkingPlace>> GetPlaces()
        {
            var policyResult = await Policy
                .Handle<Exception>()
                .WaitAndRetryAsync(5, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                (e, span) => _logger.Warning(e, "Error getting places from parking provider. Retrying..."))
                .ExecuteAndCaptureAsync(FetchPlaces);

            if (policyResult.Outcome != OutcomeType.Successful)
            {
                _logger.Error(policyResult.FinalException,
                    "Error getting places from parking provider. Giving up...");
                return new List<ParkingPlace>();
            }

            // Hardcoded randomization
            var places = policyResult.Result.Select(p =>
            {
                lock (_lock)
                {
                    var desiredMaxDelta = 11;
                    var delta = _random.Next(0, 11);
                    var newFreeCount = _random.Next(0, 2) == 0 ? p.MaxCount - delta : p.MaxCount + delta;
                    if (newFreeCount > p.MaxCount)
                    {
                        newFreeCount = p.MaxCount;
                    }
                    if (newFreeCount < 0)
                    {
                        newFreeCount = 0;
                    }
                    p.FreeCount = newFreeCount;

                }
                return p;
            });
            return places;
        }

        private async Task<IEnumerable<ParkingPlace>> FetchPlaces()
        {

            var cachedPlaces = await _cache.Get<List<ParkingPlace>>(CacheKeys.ParkingPlaces);

            if (cachedPlaces != null && cachedPlaces.Any())
            {
                _logger.Information("Retrieving parking places from cache.");
                return cachedPlaces;
            }

            _logger.Information("Fetching new parking places from provider.");
            var models = (await _httpClient
                .Request("places.json")
                .GetJsonAsync<IEnumerable<UcnParkingPlaceDto>>())
                .Select((dto,i) => dto.ToInternalModel(i+1))
                .ToList();

            _logger.Information("Saving fetched parking places to cache.");
            await _cache.Set(CacheKeys.ParkingPlaces, models);
            return models;
        }
    }
}
