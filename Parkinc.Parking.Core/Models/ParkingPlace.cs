﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Parkinc.Parking.Core.Models
{
    [DataContract]
    public class ParkingPlace
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "latitude")]
        public double Latitude { get; set; }

        [DataMember(Name = "longitude")]
        public double Longitude { get; set; }

        [DataMember(Name = "freeCount")]
        public int FreeCount { get; set; }

        [DataMember(Name = "maxCount")]
        public int MaxCount { get; set; }

        [DataMember(Name = "isOpen")]
        public bool IsOpen { get; set; }

        [DataMember(Name = "isPaymentActive")]
        public bool IsPaymentActive { get; set; }

        [DataMember(Name = "status")]
        public int Status { get; set; }
    }
}
