﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Parkinc.Parking.Core.Helpers.Interface;
using StackExchange.Redis;

namespace Parkinc.Parking.Core.Helpers
{
    public class RedisCache : IRedisCache
    {
        private readonly IDatabase _db;
        public RedisCache(IConnectionMultiplexer multiplexer)
        {
            _db = multiplexer.GetDatabase();
        }

        public async Task Set<T>(string key, T value, TimeSpan? expireIn = null)
        {
            var json = JsonConvert.SerializeObject(value);
            var expiry = expireIn ?? TimeSpan.FromDays(7);
            await _db.StringSetAsync(key, json, expiry);
        }

        public async Task<T> Get<T>(string key)
        {

            var value = await _db.StringGetAsync(key);
            if (!value.HasValue) return await Task.FromResult(default(T));
            var obj = JsonConvert.DeserializeObject<T>(value);
            return obj;
        }
    }
}
