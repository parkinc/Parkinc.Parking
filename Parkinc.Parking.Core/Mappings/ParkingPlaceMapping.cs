﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Parkinc.Parking.Core.Integrations.DTOs;
using Parkinc.Parking.Core.Models;
using Parkinc.Parking.Messages;

namespace Parkinc.Parking.Core.Mappings
{
    public static class ParkingPlaceMapping
    {
        public static ParkingPlace ToInternalModel(this UcnParkingPlaceDto dto, int? id = null)
        {
            var model = new ParkingPlace()
            {
                Name = dto.Name,
                Latitude = double.Parse(dto.Latitude, CultureInfo.InvariantCulture),
                Longitude = double.Parse(dto.Longitude, CultureInfo.InvariantCulture),
                FreeCount = int.Parse(dto.FreeCount),
                MaxCount = int.Parse(dto.MaxCount),
                IsOpen = int.Parse(dto.IsOpen) == 1,
                IsPaymentActive = int.Parse(dto.IsPaymentActive) == 1,
                Status = int.Parse(dto.Status)
            };
            if (id != null)
            {
                model.Id = id.Value;
            }
            return model;
        }

        public static ParkingPlaceV1 ToMessageModel(this ParkingPlace place)
        {
            var msg = new ParkingPlaceV1()
            {
                Name = place.Name,
                FreeCount = place.FreeCount,
                MaxCount = place.MaxCount,
                IsOpen = place.IsOpen,
                IsPaymentActive = place.IsPaymentActive,
                Status = place.Status,
                Longitude = place.Longitude,
                Latitude = place.Latitude,
            };
            return msg;
        }
    }
}
