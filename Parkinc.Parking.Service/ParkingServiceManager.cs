﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyNetQ;
using Microsoft.Extensions.Configuration;
using Parkinc.Parking.Core.Integrations.Interfaces;
using Parkinc.Parking.Core.Mappings;
using Parkinc.Parking.Messages;
using Serilog;

namespace Parkinc.Parking.Service
{
    public class ParkingServiceManager : IServiceManager
    {

        private readonly IBus _bus;
        private readonly IParkingDataProvider _parkingDataProvider;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly List<ISubscriptionResult> _subscriptions;
        private bool _isRunning;

        public ParkingServiceManager(IBus bus, IParkingDataProvider parkingDataProvider, 
                                     ILogger logger, IConfiguration configuration)
        {
            _bus = bus;
            _parkingDataProvider = parkingDataProvider;
            _logger = logger;
            _configuration = configuration;
            _subscriptions = new List<ISubscriptionResult>();
        }

        public bool Start()
        {
            _logger.Information("Starting service..");
            if (_isRunning) return false;
            try
            {
                var subId = _configuration["Messaging:SubscriptionId"];
                var subscription = _bus.SubscribeAsync<ParkingDataRequestV1>(subId, FetchAndRespond);
                _subscriptions.Add(subscription);
                _isRunning = true;
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e, "Unhandled exception while starting parking service manager.");
                return false;
            }
        }

        public bool Stop()
        {
            _logger.Information("Stopping service..");
            if (!_isRunning) return false;
            try
            {
                _subscriptions.ForEach(s => s.Dispose());
                _isRunning = false;
                _logger.Information($"Subscriptions disposed. IsRunning: {_isRunning}");
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e, "Unhandled exception while stopping parking service manager.");
                return false;
            }
        }

        private async Task FetchAndRespond(ParkingDataRequestV1 request)
        {
            try
            {
                _logger.Information("Entered FetchAndRespond");
                var places = await _parkingDataProvider.GetPlaces();
                _logger.Information("Fetched parking places");
                var msgPlaces = places.Select(x => x.ToMessageModel()).ToList();
                var responseMsg = new ParkingDataResponseV1()
                {
                    TimestampUtc = DateTime.UtcNow,
                    ParkingData = msgPlaces
                };

                await _bus.PublishAsync(responseMsg);
                _logger.Information("Responded.");
            }
            catch (Exception e)
            {
                _logger.Error(e, "Unhandled exception while handling parking data request.");
            }
        }
    }
}
