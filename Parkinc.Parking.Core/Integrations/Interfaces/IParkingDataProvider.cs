﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Parkinc.Parking.Core.Models;

namespace Parkinc.Parking.Core.Integrations.Interfaces
{
    public interface IParkingDataProvider
    {
        Task<IEnumerable<ParkingPlace>> GetPlaces();
    }
}
