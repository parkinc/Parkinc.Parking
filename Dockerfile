FROM microsoft/dotnet:runtime
COPY release /app
WORKDIR /app
ENTRYPOINT ["dotnet", "Parkinc.Parking.Service.dll"]
