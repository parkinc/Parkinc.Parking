﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parkinc.Parking.Service
{
    public interface IServiceManager
    {
        bool Start();
        bool Stop();
    }
}
