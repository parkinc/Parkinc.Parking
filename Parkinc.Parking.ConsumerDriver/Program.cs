﻿using System;
using Autofac;
using EasyNetQ;
using EasyNetQ.Loggers;
using Newtonsoft.Json;
using Parkinc.Parking.Messages;
using Serilog;

namespace Parkinc.Parking.ConsumerDriver
{
    class Program
    {
        static void Main(string[] args)
        {
            var conStr =
                "host=swan.rmq.cloudamqp.com;virtualhost=naghkhhc;username=naghkhhc;password=uRHTZEgVK7KZKcvrXe1WjgQpqUBK1y_M;prefetchcount=1";
            var builder = new ContainerBuilder();
            builder
                .Register(ctx => RabbitHutch.CreateBus(conStr, x => x.Register<IEasyNetQLogger>(_ => new ConsoleLogger())))
                .As<IBus>()
                .SingleInstance();
            builder
                .RegisterType<DriverManager>()
                .SingleInstance();
            var container = builder.Build();

            Console.WriteLine("Starting...");
            var manager = container.Resolve<DriverManager>();
            manager.Start();
        }
    }
}
