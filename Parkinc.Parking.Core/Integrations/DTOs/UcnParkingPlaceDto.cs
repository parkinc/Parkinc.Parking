﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Parkinc.Parking.Core.Integrations.DTOs
{
    [DataContract]
    public class UcnParkingPlaceDto
    {
        [DataMember(Name="name")]
        public string Name { get; set; }
        [DataMember(Name="latitude")]
        public string Latitude { get; set; }
        [DataMember(Name="longitude")]
        public string Longitude { get; set; }
        [DataMember(Name="free_count")]
        public string FreeCount { get; set; }
        [DataMember(Name="max_count")]
        public string MaxCount { get; set; }
        [DataMember(Name="is_open")]
        public string IsOpen { get; set; }
        [DataMember(Name="is_payment_active")]
        public string IsPaymentActive { get; set; }
        [DataMember(Name="status_park_place")]
        public string Status { get; set; }
    }
}
