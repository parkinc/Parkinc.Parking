﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parkinc.Parking.Messages
{
    public class ParkingPlaceV1
    {
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int FreeCount { get; set; }
        public int MaxCount { get; set; }
        public bool IsOpen { get; set; }
        public bool IsPaymentActive { get; set; }
        public int Status { get; set; }
    }
}
