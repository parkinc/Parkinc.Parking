﻿using System;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using Autofac;
using Flurl.Http;
using Parkinc.Parking.Core.Integrations;
using Serilog;

namespace Parkinc.Parking.Service
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = Bootstrap.InitializeContainer();
            var manager = container.Resolve<IServiceManager>();
            Log.Information("Testing structured logging: AppName: {ApplicationName} Machine Name: {MachineName}");
            manager.Start();

            AssemblyLoadContext.Default.Unloading += context => manager.Stop(); // Experimental
        }
    }
}
