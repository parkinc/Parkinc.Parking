﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Parkinc.Parking.Core.Helpers.Interface
{
    public interface IRedisCache
    {
        Task Set<T>(string key, T value, TimeSpan? expireIn = null);
        Task<T> Get<T>(string key);
    }
}
