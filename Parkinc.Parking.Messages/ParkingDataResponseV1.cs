﻿using System;
using System.Collections.Generic;
using System.Text;
using EasyNetQ;

namespace Parkinc.Parking.Messages
{
    [Queue("Q_ParkingDataResponse", ExchangeName = "E_ParkingDataResponse")]
    public class ParkingDataResponseV1
    {
        public DateTime TimestampUtc { get; set; }
        public IEnumerable<ParkingPlaceV1> ParkingData { get; set; }
    }
}
