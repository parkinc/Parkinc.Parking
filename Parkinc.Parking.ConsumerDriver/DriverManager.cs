﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EasyNetQ;
using Newtonsoft.Json;
using Parkinc.Parking.Messages;

namespace Parkinc.Parking.ConsumerDriver
{
    public class DriverManager
    {
        private ISubscriptionResult _sub;
        private readonly IBus _bus;

        public DriverManager(IBus bus)
        {
            _bus = bus;
        }
        public void Start()
        {
            _sub = _bus.SubscribeAsync<ParkingDataResponseV1>("ParkingServiceDataRequest", Consume);
        }

        private async Task Consume(ParkingDataResponseV1 msg)
        {
            Console.WriteLine("Response consumed.");
            Console.WriteLine(JsonConvert.SerializeObject(msg, Formatting.Indented));
            await Task.FromResult(0);
        }
    }
}
