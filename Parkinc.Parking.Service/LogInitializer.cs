﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Serilog;
using Serilog.Configuration;
using Serilog.Sinks.Elasticsearch;

namespace Parkinc.Parking.Service
{
    public static class LogInitializer
    {
        public static ILogger InitializeLogger(string logzioToken, string elasticSerachUri)
        {
            var config = new LoggerConfiguration()
                .Enrich.WithProperty("ApplicationName", Assembly.GetExecutingAssembly().GetName().Name)
                .Enrich.WithMachineName();

            config.WriteTo.Console();
            config.WriteTo.Logzio(logzioToken);
            config.WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(elasticSerachUri)));
            return Log.Logger = config.CreateLogger();
        }
    }
}
